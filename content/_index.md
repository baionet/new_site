---
title: Accueil
date: 2020-03-23
menu:
  - main
  - sidebar
weight: -270


---

# Libérez votre internet

Baionet est un fournisseur d'accès a internet associatif basé a Bayonne au pays basque.   
Nous millitons pour un internet neutre.  
Pour cela,nous fournissons des services que vous pouvez retrouver dans la page "[services](/services)"
