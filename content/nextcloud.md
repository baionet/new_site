---
title: Nextcloud
date: 2020-03-23
weight: -270

---
Nous proposons une instance de nextcloud, une alternative a google drive.   
L'instance nextcloud propose les fonctions suivantes :   

* Stockage de fichiers
* Stockage de photos
* Discussion avec des personnes ayant un compte sur le nextcloud Baionet
* Client mail intégré,nous ne fournissons pour l'instant pas d'adresse mail
* Stockage de vos contacts
* Stockage de votre agenda
* [Kanban](https://fr.wikipedia.org/wiki/Kanban)

Le quota par défaut est de 2go avec possibilité d'extension sur demande.   
Le service est inclus dans l'adhésion (mais sur demande).   
