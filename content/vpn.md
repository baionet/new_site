---
title: VPN
date: 2020-03-23
weight: -270

---

#### Comment s’y abonner ?
Si vous êtes membre de l’association, il suffit de commander le VPN auprès de l’association.
Vous pouvez aussi adhérer et commander un VPN en même temps.

#### À quoi ça sert ?
Un dessin est souvent la solution la plus parlante. On peut y voir deux exemples d’accès :

Un accès standard (en haut), ne transitant que par le FAI. Dans ce cas, le FAI voit le trafic qui passe chez lui.
Un accès par le VPN Baionet (en bas). Le FAI ne voit alors que des données chiffrées, sans moyen d’en identifier le contenu ou la provenance.

![Schéma d’un VPN](vpn-scheme.jpg)

#### Combien ça coûte ?
Il existe 2 tarifs :
*  Le tarif normal (2,50€)
*  Le tarif réduit pour les chomeurs,les étudiants et les bénéficiaires des minimas sociaux (1.20€).
Les tarif sont exprimés au mois.


#### Comment le configurer ?
Tout est expliqué sur la [page wiki de la configuration VPN](https://wiki.baionet.fr/doku.php?id=client-vpn)

#### Suis-je protégé si j’ai recours à des pratiques illégales ?
Non. Si une adresse IP du VPN est « flashée » dans des activités illégales, la justice se tournera vers Baionet qui sera légalement forcé de fournir l’adresse IP ayant utilisé le VPN ainsi que toute autre information en sa possession reliée à cette IP. La loi, juste la loi. Ni plus, ni moins.

#### Est-ce que tous les ports sont ouverts ?
Oui, les adresses IP v4 et v6 fournies via ce VPN sont complètement ouvertes sur tous les ports, nous ne faisons aucun filtrage. Bref, un vrai accès Internet.

#### S’agit-il d’un VPN Baionet ou d’un VPN FDN ?
Le VPN est bien opéré techniquement par Baionet , son infrastructure est basée à Pau. FDN propose également ce service, tout comme d’autres FAI associatifs de la fédération FFDN, voir http://www.ffdn.org/fr/membres

#### Qu’est-ce qui est chiffré pour mon FAI actuel (URL, contenu, etc) ?
Par défaut, tout passe par le VPN, et donc votre FAI actuel ne verra qu’un flux de paquets à destination de Baionet, sans avoir aucun moyen de savoir ce que ces paquets contiennent ni à qui ils sont réellement destinés. C’est pour cette raison que cette technique se nomme « tunnel VPN ».

#### Peut-on choisir ce qui passe par le VPN ?
Oui. Vous pouvez aussi choisir de ne faire passer par le VPN que les communications avec certains sites, les autres continuant à passer normalement. Il faudra alors juste faire attention de aussi faire passer la résolution DNS, c’est-à-dire la traduction entre le nom de domaine du site auquel vous accédez en adresse IP, par le VPN, sinon votre FAI actuel pourrait savoir à quel site vous accédez (sans connaître le contenu).

#### Et du coup tout est chiffré partout ?
Oui et non. Tout sera chiffré entre vous et Baionet seulement. Votre FAI actuel ne verra effectivement donc que des données chiffrées. Mais entre Baionet et le site avec lequel vous communiquez, par contre, les communications ne seront pas chiffrées, à moins que vous utilisiez un protocole chiffré de bout en bout (HTTPs, IMAPs, etc.)

#### Quelles informations Baionet conserve sur mes connexions via le VPN ?
Ce que dit actuellement la loi : à quel moment quelle adresse IP a été utilisée par lequel de nos adhérents, et depuis quelle adresse IP. Si la justice fait une demande en bonne et dûe forme, nous fournirons ces renseignements.

#### Tor, VPN ou les deux ? Quelle est la différence ?
En fait, on pourrait dire que Tor c’est empiler des VPNs les uns sur les autres. Avec un seul VPN, le juge peut demander au fournisseur du VPN quelle adresse IP se cache derrière l’adresse IP du VPN. On peut commander plusieurs VPN et les empiler pour que le juge ait à remonter les différentes adresses IP. Tor fait cela automatiquement, et ne nécessite pas d’abonnement.

#### Le VPN me protège-t-il davantage contre les atteintes aux libertés fondamentales induites par la Loi Renseignement ?
Il évite une éventuelle sonde qui serait placée chez votre FAI. Il n’évite cependant pas une sonde qui serait placée entre Baionet et le site avec lequel vous communiquez. L’adresse IP exposée est cependant celle que vous aura attribué le serveur de VPN, et seul ce serveur saura qui est derrière, et seul un juge peut demander à avoir ce renseignement.

#### Avec quels autres services je peux combiner mon accès VPN pour m’assurer plus d’anonymat et le secret de mes correspondances ?
Être réellement anonyme est très difficile, car il y aura toujours l’adresse IP source de vos communications. Seul Tor fournit un moyen de brouiller les pistes à l’aide d’un empilement de changements d’adresses IP qui est difficile à éplucher.
Le secret des correspondances est par contre plus facile à garantir : il suffit de chiffrer les correspondances, c’est-à-dire d’utiliser HTTPs au lieu de HTTP, de chiffrer les mails à l’aide d’OpenPGP, etc.

#### Le serveur VPN sauvegarde-t-il mes informations de connexion (URL visitées, etc) ?
Non. Rien n’est stocké sur le serveur VPN hormis la correspondance entre votre adresse IP et l’adresse IP que le VPN vous a attribué.

#### Est-ce qu’on peut se connecter à un VPN à partir de tout appareil ou y a-t-il des restrictions ?
Il suffit que l’appareil ait le logiciel Wireguard.
On peut le trouver dans n’importe quelle distribution Linux, BSD, ainsi que sous Windows, macOS, et même des smartphones android et iPhone.

#### Peut-on installer un VPN sur un serveur où tournent déjà d’autres services (serveur apache, mail, …) ?
Oui,sans problèmes.

#### Quel impact aura l’utilisation d’un VPN sur la vitesse de ma connexion ?
En théorie le surcoût du VPN lui-même est assez léger : juste un peu plus de latence pour passer par le serveur VPN et chiffrer les données. En pratique par contre, la bande passante se retrouve limitée à celle dont dispose le serveur VPN, mais celle-ci est proportionnelle à l’argent que l’on peut y dépenser, donc notamment au nombres d’abonnés. Bien sûr, plus il y a d’abonnés, plus il y a de consommation, mais l’expérience montre qu’en moyenne un abonné ne consomme pas beaucoup, et donc avoir beaucoup d’abonnés permet en fait d’avoir une bande passante totale largement suffisante.

#### Est-ce qu’un VPN consomme plus de données (Fair-use des abonnements de téléphone portable) ?
Il y a deux surcoûts :

    * Pour chaque paquet de données envoyé, il y a le coût de l’encapsulation, que l’on peut chiffrer à quelques pourcents seulement, ce qui est donc relativement négligeable.

    * Pour garder la connexion ouverte, le serveur envoie régulièrement des acquittements. Notre réglage est actuellement à un acquittement de 81 octets émis et reçu toutes les 5 secondes. Cela consomme donc 60Ko/heure, ou 1,4Mo/jour. C’est l’équivalent d’un email par jour avec photo en pièce-jointe, ou encore de l’accès à une page web avec beaucoup d’images. Sur un forfait mobile à 20€ c’est négligeable ; mais sur un forfait Free à 2€ c’est plus de 50% du forfait mensuel, il vaut alors mieux éviter de le garder connecté en permanence.

#### Est-ce que j’aurai une IP fixe ?
Oui, nous fournissons une IP fixe pour tous les adhérents VPN

#### Est-ce que je peux avoir le reverse DNS que je veux sur mon IP statique ?
Les détails seront communiqués prochainement

#### Est-ce qu’on peut être anonyme en achetant un VPN avec des bitcoins ou des cartes prépayées ?
Nous ne prenons pas actuellement ces moyens de paiement.
Il est possible par contre d’utiliser un VPN « openbar » tel que celui fourni par FDN : le serveur VPN accepte n’importe quel login/mot de passe, et ne connait donc que l’adresse IP à partir de laquelle on s’est connecté. Le coût du serveur est financé par des dons, qui même s’ils ne sont pas forcément anonymes, ne sont pas liés à l’utilisation faite du serveur VPN.
